import request from "../utils/request";
import qs from "qs";

export const postLoginMbApi = (params) => {
  return request({
    url: "/users/login.jsp",
    method: "post",
    data: qs.stringify(params),
  });
};
