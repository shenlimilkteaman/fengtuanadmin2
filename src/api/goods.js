import request from "../utils/request";
import qs from "qs";

export const getgoodsApi = (params) => {
  return request({
    url: "/goods/index.jsp",
    method: "get",
    params: qs.stringify(params),
  });
};
export const deletegoodsApi = (params) => {
  return request({
    url: "/goods/delete.jsp",
    method: "delete",
    params: qs.stringify(params),
  });
};
