import request from "../utils/request";
import qs from "qs";

export const getUsersApi = (params) => {
  return request({
    url: "/users/index.jsp",
    method: "get",
    params: qs.stringify(params),
  });
};
export const deleteUsersApi = (params) => {
  return request({
    url: "/users/delete.jsp",
    method: "delete",
    params: qs.stringify(params),
  });
};
export const UsercreateApi = (params) => {
  return request({
    url: "/users/create.jsp",
    method: "post",
    data: qs.stringify(params),
  });
};
