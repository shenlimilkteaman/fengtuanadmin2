// 导入样式
import { Container } from "./styled";

// 导入API

// 导入组件
import Top from "./components/top";
import { MenuOutlined, SettingOutlined } from "@ant-design/icons";
import { Layout, Menu } from "antd";
import React, { useState } from "react";
import { Outlet, useNavigate } from "react-router-dom";
const { Header, Sider, Content } = Layout;

const LayoutComponent = () => {
  // 状态
  const [collapsed, setCollapsed] = useState(false);
  // 路由
  const navigator = useNavigate();

  // 返回JSX
  return (
    <Container>
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu
            theme="dark"
            mode="inline"
            defaultSelectedKeys={["1"]}
            // onClick={({ item, key, keyPath, domEvent }) => {
            onClick={({ keyPath }) => {
              // console.log(item, key, keyPath, domEvent);
              // console.log(domEvent);
              let urls = {
                "1-0": "/admin",
                "2-1": "/admin/users",
                "2-2": "/admin/users/create",
                "3-1": "/admin/goods",
                "3-2": "/admin/goods/create",
              };
              let urlsKey = keyPath.shift();

              // console.log(urls, urlsKey, urls[urlsKey]);
              navigator(urls[urlsKey]);
            }}
            items={[
              {
                key: "1-0",
                icon: <MenuOutlined />,
                label: "后台首页",
              },
              {
                key: "2-0",
                icon: <MenuOutlined />,
                label: "用户管理",
                children: [
                  {
                    key: "2-1",
                    icon: <SettingOutlined />,
                    label: "用户列表",
                  },
                  {
                    key: "2-2",
                    icon: <SettingOutlined />,
                    label: "用户创建",
                  },
                ],
              },

              {
                key: "3-0",
                icon: <MenuOutlined />,
                label: "门店商品",
                children: [
                  {
                    key: "3-1",
                    icon: <SettingOutlined />,
                    label: "商品列表",
                  },
                  {
                    key: "3-2",
                    icon: <SettingOutlined />,
                    label: "商品创建",
                  },
                ],
              },
            ]}
          />
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
              padding: 0,
            }}
          >
            <Top collapsed={collapsed} setCollapsed={setCollapsed} />
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
              overflowY: "scroll",
            }}
          >
            <Outlet></Outlet>
          </Content>
        </Layout>
      </Layout>
    </Container>
  );
};

export default LayoutComponent;
