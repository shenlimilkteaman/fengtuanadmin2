import styled from "styled-components";

export const Container = styled.div`
  height: 100%;
  .ant-layout {
    height: 100%;

    header {
      background-color: #fff;

      display: flex;

      .left,
      .right {
        display: flex;
        align-items: center;
      }

      .left {
        width: 70%;
        height: 64px;
        justify-content: flex-start;
        /* background-color: green; */

        .menuIcon {
          width: 50px;
          text-align: center;
        }
      }
      .right {
        width: 30%;
        height: 64px;
        justify-content: flex-end;
        /* background-color: blue; */
      }
    }
  }
`;
