const proxy = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    proxy("/api", {
      target: "http://kg.zhaodashen.cn/mt/admin/", // 目标服务器网址
      changeOrigin: true, // 是否允许跨域
      secure: false, // 关闭SSL证书验证HTTPS接口需要
      pathRewrite: {
        // 重写路径请求
        "^/api": "",
      },
    })
  );

  // ...
};
