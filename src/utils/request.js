// baseURL
// const baseURL = "http://kg.zhaodashen.cn/mt/admin/";
const baseURL = "/api";

// 定义request方法
function request(options) {
  // 令牌
  let token =
    localStorage.getItem("token") || "9201591ba0eb36c8abaea2854274f5082";

  // 解构
  let { url, method = "get", params = "", data = "", headers = {} } = options;

  // 重置
  let defaultOptions = {
    method,
    headers: {
      "content-type": "application/x-www-form-urlencoded",
      token,
      ...headers,
    },
  };
  if (data) defaultOptions.body = data;

  // 返回promise对象
  return new Promise((resolve, reject) => {
    return fetch(baseURL + url + `?${params}`, defaultOptions)
      .then((res) => res.json())
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

// 导出request对象
export default request;
