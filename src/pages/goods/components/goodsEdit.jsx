import { Form, Select, Input } from "antd";
import { Container } from "./goodsEdit.styled";
const { Option } = Select;
function Edit() {
  const onFinish = async (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <Container>
      <Form
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          name="question"
          rules={[
            {
              required: true,
              message: "请选择密保问题",
            },
          ]}
        >
          <Select placeholder="请选择密保问题">
            <Option value="你爷爷的名字">你爷爷的名字</Option>
            <Option value="您其中一位老师的名字">您其中一位老师的名字</Option>
            <Option value="你奶奶的名字">你奶奶的名字</Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="answer"
          rules={[
            {
              required: true,
              message: "请选择密保答案",
            },
          ]}
        >
          <Input placeholder="请输入密保答案"></Input>
        </Form.Item>
        <Form.Item
          name="uname"
          rules={[
            {
              required: true,
              message: "请输入用户名",
            },
          ]}
        >
          <Input placeholder="请输入用户名"></Input>
        </Form.Item>
        <Form.Item
          name="pwd"
          rules={[
            {
              required: true,
              message: "请输入密码",
            },
          ]}
        >
          <Input placeholder="请输入密码"></Input>
        </Form.Item>
      </Form>
    </Container>
  );
}
export default Edit;
