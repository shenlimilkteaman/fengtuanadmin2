import { fromJS } from "immutable";

const iniData = fromJS({
  tableData: {
    list: [],
    total: 0,
  },
});

const reducer = (state = iniData, action) => {
  switch (action.type) {
    case "GOOD":
      return state.setIn(["tableData"], action.payload);
    default:
      break;
  }
  return state;
};

export default reducer;
