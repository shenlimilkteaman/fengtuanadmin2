import { getgoodsApi } from "../../../api/goods";

export const getgoodsAction = (params) => {
  return async (dispatch) => {
    let res = await getgoodsApi(params);
    res.data.total = parseInt(res.data.total);
    dispatch({
      type: "GOOD",
      payload: res.data, // {list,total}
    });
  };
};
