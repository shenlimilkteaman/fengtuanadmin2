// 导入样式
import { Container } from "./styled";

import { getgoodsAction } from "./store/actionCreator";
import { connect } from "react-redux";
import { Card, Button, Table, Modal, message, Popconfirm } from "antd";

import { useEffect, useState } from "react";

import GoodsEdit from "./components/goodsEdit";
import { deletegoodsApi } from "../../api/goods";
function Goods(props) {
  const confirm = async (e) => {
    console.log(e);
    let res = await deletegoodsApi(e);
    if (res.meta.state === 200) {
      message.success(res.meta.msg);
      props.handleInitData(params);
      // navigate("/admin/users");
    } else {
      message.error(res.meta.msg);
    }
  };

  const cancel = (e) => {
    console.log(e);
  };

  const [isModalVisible, setIsModalVisible] = useState(false);

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  let [params] = useState({
    pagenum: 1,
    pagesize: 10,
  });
  useEffect(() => {
    props.handleInitData(params);
  });
  // const data = [
  //   {
  //     key: "1",
  //     name: "Joe Black",
  //     img: "John Brown",
  //     title: "New York No. 1 Lake Park",
  //     tags: ["nice", "developer"],
  //   },
  //   {
  //     key: "2",
  //     img: "Jim Green",
  //     title: "London No. 1 Lake Park",
  //     tags: ["loser"],
  //   },
  //   {
  //     key: "3",
  //     name: "Joe Black",
  //     title: "Sidney No. 1 Lake Park",
  //     tags: ["cool", "teacher"],
  //   },
  // ];

  const columns = [
    {
      title: "编号",
      dataIndex: "goods_id",
      key: "goods_id",
      render: (text) => <a href="sdf">{text}</a>,
    },
    {
      title: "封面",
      dataIndex: "goods_img",
      key: "goods_img",
      render: (text) => <img src={text} alt="" />,
    },
    {
      title: "标题",
      dataIndex: "goods_name",
      key: "goods_name",
    },
    {
      title: "库存",
      dataIndex: "goods_number",
      key: "goods_number",
    },
    {
      title: "市场价",
      dataIndex: "market_price",
      key: "market_price",
    },
    {
      title: "促销价",
      dataIndex: "shop_price",
      key: "shop_price",
    },
    {
      title: "创建于",
      dataIndex: "create_time",
      key: "create_time",
    },
    {
      title: "操作",
      key: "action",
      render: (text, record, index) => {
        // console.log(text, record, index);
        return (
          <>
            <Button
              type="primary"
              style={{ marginRight: "10px" }}
              onClick={() => {
                console.log(text, record, index);
                setIsModalVisible(true);
              }}
            >
              修改商品
            </Button>
            <Popconfirm
              title="确定要删除当前行数据吗"
              onConfirm={() => confirm(record)}
              onCancel={cancel}
              okText="删除"
              cancelText="取消"
            >
              <Button type="primary" danger>
                删除
              </Button>
            </Popconfirm>
          </>
        );
      },
    },
  ];
  return (
    <Container>
      <Card title="商品列表" extra={<a href="/admin/goods/create">创建</a>}>
        {/* 对话框 */}
        <Modal
          title="修改商品"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <GoodsEdit></GoodsEdit>
        </Modal>
        {/* 对话框 */}
        <Table
          columns={columns}
          dataSource={props.tableData}
          pagination={false}
          rowKey="goods_id"
        />
      </Card>
    </Container>
  );
}
const mapStateToProps = (state) => {
  console.log("拿到数据：", state.toJS());
  return {
    tableData: state.toJS().goods.tableData.list,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleInitData: (params) => dispatch(getgoodsAction(params)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Goods);
