import styled from "styled-components";
export const Container = styled.div`
  .ant-table {
    .ant-table-cell {
      width: 50px;
      height: 50px;
      img {
        width: 50px;
        height: 50px;
      }
    }
  }
`;
