import { getUsersApi } from "../../../api/users";

export const getUsersAction = (params) => {
  return async (dispatch) => {
    let res = await getUsersApi(params);
    res.data.total = parseInt(res.data.total);
    dispatch({
      type: "USERS/SET_TABLEDATA",
      payload: res.data, // {list,total}
    });
  };
};
// export const deleteUsersAction = (params) => {
//   return async (dispatch) => {
//     let res = await deleteUsersApi(params);
//     res.data.total = parseInt(res.data.total);
//     dispatch({
//       type: "USERS/DELETE",
//       payload: res.data, // {list,total}
//     });
//   };
// };
