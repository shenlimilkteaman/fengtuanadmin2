// 导入样式
import { Container } from "./userEdit.styled";
// 导入API
import { useNavigate } from "react-router-dom";
// 导入组件
import { Modal, Form, Select, Input, Button, message } from "antd";
const { Option } = Select;

function UserEdit(props) {
  const [form] = Form.useForm();

  // form.setFieldValue({
  //   uname: props.uname,
  // });

  // 路由
  const navigate = useNavigate();

  // 表单
  const onFinish = (values) => {
    console.log("Success:", values);

    message.success("编辑成功");
    navigate("/admin");
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const handleOk = () => {
    console.log("确定：改变父的状态");
    form.submit();
    // props.close();
  };

  const handleCancel = () => {
    console.log("取消：改变父的状态");
    props.close();
  };

  // console.log("当前行数据：", props);
  return (
    <Modal
      title="用户编辑"
      destroyOnClose={true}
      visible={props.state}
      onOk={handleOk}
      onCancel={handleCancel}
    >
      <Container>
        <Form
          form={form}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          initialValues={{
            uname: props.row.uname,
          }}
        >
          <Form.Item
            name="question"
            rules={[
              {
                required: true,
                message: "请选择密保问题",
              },
            ]}
          >
            <Select placeholder="请选择密保问题">
              <Option value="你爷爷的名字">你爷爷的名字</Option>
              <Option value="您其中一位老师的名字">您其中一位老师的名字</Option>
              <Option value="你奶奶的名字">你奶奶的名字</Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="answer"
            rules={[
              {
                required: true,
                message: "请输入密保答案",
              },
            ]}
          >
            <Input placeholder="请输入密保答案" />
          </Form.Item>
          <Form.Item
            name="uname"
            rules={[
              // {
              //   required: true,
              //   message: "请输入用户名",
              // },

              {
                validator: (_, value) => {
                  console.log(111);
                  if (!value) return Promise.reject(new Error("请输入用户名"));
                  if (value.length < 2 || value.length > 10) {
                    return Promise.reject(new Error("密码至少2-10个字符"));
                  }
                  return Promise.resolve();
                },
                // validateTrigger: "onBlur",
              },
            ]}
          >
            <Input placeholder="请输入用户名" />
          </Form.Item>
          <Form.Item
            name="pwd"
            rules={[
              {
                required: true,
                message: "请输入密码",
              },
            ]}
          >
            <Input placeholder="请输入密码" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              更新
            </Button>
          </Form.Item>
        </Form>
      </Container>
    </Modal>
  );
}

export default UserEdit;
