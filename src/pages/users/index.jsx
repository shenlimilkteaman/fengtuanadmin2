// 导入样式
import { Container } from "./styled";
// 导入API
import { getUsersAction } from "./store/actionCreator";
import { connect } from "react-redux";
import { deleteUsersApi } from "../../api/users";

// 导入组件
import UserEdit from "./components/userEdit";
import {
  FormOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  Card,
  Button,
  Input,
  DatePicker,
  Table,
  Pagination,
  Modal,
  message,
} from "antd";
import { useEffect, useState } from "react";
const { Search } = Input;
const { RangePicker } = DatePicker;

const onChange = (value, dateString) => {
  console.log("Selected Time: ", value);
  console.log("Formatted Selected Time: ", dateString);
};

const onOk = (value) => {
  console.log("onOk: ", value);
};
const itemRender = (_, type, originalElement) => {
  return originalElement;
};

// const handleDel = (row) => {
//   console.log("del：", row);

//   Modal.confirm({
//     title: "确定删除吗",
//     icon: <ExclamationCircleOutlined />,
//     // content: "Some descriptions",
//     async onOk() {

//       console.log("OK", row.user_id);
//       let res = await deleteUsersApi(row);
//       if (res.meta.state === 200) {
//         message.success(res.meta.msg);
//         navigate("/");
//       } else {
//         message.error(res.meta.msg);
//       }
//     },

//     onCancel() {
//       console.log("Cancel");
//     },
//   });
// };

function Users(props) {
  const handleDel = (row) => {
    console.log("del：", row);

    Modal.confirm({
      title: "确定删除吗",
      icon: <ExclamationCircleOutlined />,
      // content: "Some descriptions",
      async onOk() {
        console.log("OK", row.user_id);
        let res = await deleteUsersApi(row);
        if (res.meta.state === 200) {
          message.success(res.meta.msg);
          props.handleInitData(params);
          // navigate("/admin/users");
        } else {
          message.error(res.meta.msg);
        }
      },

      onCancel() {
        console.log("Cancel");
      },
    });
  };

  // 用户编辑状态
  let [userEditState, setUserEditState] = useState(false);
  let [userEditRow, setUserEditRow] = useState({});
  // 表格列
  const columns = [
    {
      title: "编号",
      dataIndex: "user_id",
      key: "user_id",
    },
    {
      title: "姓名",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "住址",
      dataIndex: "address",
      key: "address",
    },

    {
      title: "操作",
      key: "operation",
      fixed: "right",
      width: 150,
      render: (text, record, index) => {
        // console.log(text, record, index);
        return (
          <>
            <Button
              style={{ marginRight: "10px" }}
              onClick={() => {
                console.log(text, record, index);
                setUserEditState(true);

                setUserEditRow(record);
              }}
            >
              <FormOutlined />
            </Button>
            <Button onClick={() => handleDel(record)}>
              <DeleteOutlined />
            </Button>
          </>
        );
      },
    },
  ];

  const onSearch = (value) => {
    console.log(value);
  };

  let [params, setParams] = useState({
    pagenum: 1,
    pagesize: 10,
  });
  useEffect(() => {
    props.handleInitData(params);
    // eslint-disable-next-line
  }, [params.pagenum]);

  const onPage = (page, pageSize) => {
    console.log(page, pageSize);

    setParams({
      ...params,
      pagenum: page,
    });
  };

  return (
    <Container>
      <Card title="用户列表" extra={<Button type="primary">创建</Button>}>
        {/* 对话框 */}
        <UserEdit
          state={userEditState}
          row={userEditRow}
          close={() => setUserEditState(false)}
        />
        {/* 对话框 */}

        {/* 筛选 */}
        <div className="filter">
          <Search
            placeholder="请输入用户名"
            onSearch={onSearch}
            enterButton
            style={{ width: "300px" }}
          />
          &nbsp;&nbsp;
          <RangePicker
            showTime={{ format: "HH:mm" }}
            format="YYYY-MM-DD HH:mm"
            onChange={onChange}
            onOk={onOk}
          />
        </div>
        {/* 筛选 */}

        {/* 表格 */}
        <Table
          dataSource={props.tableData.list}
          columns={columns}
          pagination={false}
          rowKey="user_id"
        />
        {/* 表格 */}

        {/* 分页 */}
        <Pagination onChange={onPage} total={500} itemRender={itemRender} />
        {/* 分页 */}
      </Card>
    </Container>
  );
}

const mapStateToProps = (state) => {
  console.log("拿到数据：", state.toJS());
  return {
    tableData: state.toJS().users.tableData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleInitData: (params) => dispatch(getUsersAction(params)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Users);
