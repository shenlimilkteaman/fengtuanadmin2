// 导入样式
import { Container } from "./create.styled";
// 导入API
import { useNavigate } from "react-router-dom";

import { UsercreateApi } from "../../api/users";
// 导入组件
import { Card, Form, Select, Input, Button, message } from "antd";
const { Option } = Select;

function UserCreate() {
  // 路由
  const navigate = useNavigate();

  // 表单登录
  const onFinish = async (values) => {
    console.log("Success:", values);
    let res = await UsercreateApi(values);
    if (res.meta.state === 201) {
      message.success(res.meta.msg);
      navigate("/admin/users");
    } else {
      message.error(res.meta.msg);
    }
    // message.success("登录成功");
    // navigate("/admin");
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Container>
      <Card title="用户创建" extra={<Button type="primary">返回</Button>}>
        <Form
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            name="question"
            rules={[
              {
                required: true,
                message: "请选择密保问题",
              },
            ]}
          >
            <Select placeholder="请选择密保问题">
              <Option value="你爷爷的名字">你爷爷的名字</Option>
              <Option value="您其中一位老师的名字">您其中一位老师的名字</Option>
              <Option value="你奶奶的名字">你奶奶的名字</Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="answer"
            rules={[
              {
                required: true,
                message: "请选择密保答案",
              },
            ]}
          >
            <Input placeholder="请选择密保答案" />
          </Form.Item>

          <Form.Item
            name="username"
            rules={[
              // {
              //   required: true,
              //   message: "请输入用户名",
              // },

              {
                validator: (_, value) => {
                  console.log(111);
                  if (!value) return Promise.reject(new Error("请输入用户名"));
                  if (value.length < 2 || value.length > 10) {
                    return Promise.reject(new Error("至少2-10个字符"));
                  }
                  return Promise.resolve();
                },
                // validateTrigger: "onBlur",
              },
            ]}
          >
            <Input placeholder="请输入用户名" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "请输入密码",
              },
            ]}
          >
            <Input placeholder="请输入密码" />
          </Form.Item>
          <Form.Item
            name="mobile"
            rules={[
              {
                required: true,
                message: "请输入手机号",
              },
            ]}
          >
            <Input placeholder="请输入手机号" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              创建
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </Container>
  );
}

export default UserCreate;
