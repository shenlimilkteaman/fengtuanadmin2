// 导入样式
import { Container } from "./styled";
//导入组件
import { Form, Select, Input, Button, message } from "antd";

import { postLoginMbApi } from "../../api/login";
import { useNavigate } from "react-router-dom";
const { Option } = Select;
function Login() {
  // 路由
  const navigate = useNavigate();

  const onFinish = async (values) => {
    console.log("Success:", values);
    let res = await postLoginMbApi(values);
    if (res.meta.state === 200) {
      localStorage.setItem("uname", res.data.uname);
      localStorage.setItem("roleName", res.data.roleName);
      localStorage.setItem("token", res.data.token);
      message.success(res.meta.msg);
      navigate("/admin");
    } else {
      message.error(res.meta.msg);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <Container>
      <Form
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item>
          <h1>封团本地生活平台</h1>
        </Form.Item>
        <Form.Item
          name="question"
          rules={[
            {
              required: true,
              message: "请选择密保问题",
            },
          ]}
        >
          <Select placeholder="请选择密保问题">
            <Option value="你爷爷的名字">你爷爷的名字</Option>
            <Option value="您其中一位老师的名字">您其中一位老师的名字</Option>
            <Option value="你奶奶的名字">你奶奶的名字</Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="answer"
          rules={[
            {
              required: true,
              message: "请选择密保答案",
            },
          ]}
        >
          <Input placeholder="请输入密保答案"></Input>
        </Form.Item>
        <Form.Item
          name="uname"
          rules={[
            {
              required: true,
              message: "请输入用户名",
            },
          ]}
        >
          <Input placeholder="请输入用户名"></Input>
        </Form.Item>
        <Form.Item
          name="pwd"
          rules={[
            {
              required: true,
              message: "请输入密码",
            },
          ]}
        >
          <Input placeholder="请输入密码"></Input>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            登录
          </Button>
        </Form.Item>
      </Form>
    </Container>
  );
}

export default Login;
